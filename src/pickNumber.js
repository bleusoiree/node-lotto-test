'use strict';

const MAX_SIZE = 45,
    PICK_SIZE = 6,
    { log, time, timeEnd } = console,
    winnerLog = new Map();

function getRandomNumber() {
    return Math.floor(Math.random() * MAX_SIZE);
}

module.exports.pick = (numberSize, winNumber) => {
    const pickedSet = new Set();

    if (numberSize <= 0)
        return new Error('number size must be bigger then zero.');

    while (pickedSet.size < numberSize) {
        pickedSet.add(getRandomNumber());
    }

    const sortedLottoArray = Array.from(pickedSet).sort((a, b) => a - b);
    const result =
        sortedLottoArray.toString() ===
        winNumber.sort((a, b) => a - b).toString();

    return {
        result,
        sortedLottoArray
    };
};

module.exports.loopAndMatch = (loopSize, winNumber) => {
    time('elapsed time for lotto pick');
    for (let i = 1; i <= loopSize; i++) {
        if (i % 10000 === 0) log(`processing... [${loopSize}/${i}]`);

        const pickedLotto = this.pick(PICK_SIZE, winNumber);
        if (pickedLotto.result === true) {
            winnerLog.set(i, {
                result: pickedLotto.result,
                lotto: pickedLotto.sortedLottoArray,
                winDate: new Date()
            });
        }
    }

    timeEnd('elapsed time for lotto pick');
    log(`win size: ${winnerLog.size}, winner info: `, winnerLog);
};
